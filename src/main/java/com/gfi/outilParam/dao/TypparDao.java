package com.gfi.outilParam.dao;

import java.sql.SQLException;
import java.util.List;

import com.gfi.outilParam.business.Typpar;

public interface TypparDao {

	// Read column names
	public List<Typpar> findAllColumnNames() throws SQLException;
}
