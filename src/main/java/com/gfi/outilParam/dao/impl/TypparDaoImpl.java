package com.gfi.outilParam.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gfi.outilParam.business.Typpar;
import com.gfi.outilParam.dao.Requetes;
import com.gfi.outilParam.dao.TypparDao;
import com.gfi.outilParam.utils.DataConnect;

@Repository
public class TypparDaoImpl implements TypparDao {

	private static List<Typpar> typpars = new ArrayList<>();

	private Connection connection;

	public TypparDaoImpl() {
		try {
			this.connection = DataConnect.getConnection();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@Override
	public List<Typpar> findAllColumnNames() throws SQLException {
		java.sql.PreparedStatement ps = connection.prepareStatement(Requetes.ALL_COLUMN_NAMES);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Typpar typpar = new Typpar();
			typpar.setId(rs.getFloat(1));
			typpar.setCodeTypePara(rs.getString(2));
			typpar.setIdCatePara(rs.getFloat(3));
			typpar.setIdNatuPara(rs.getFloat(4));
			typpar.setIdFormPara(rs.getFloat(5));
			typpar.setLibelle(rs.getString(6));
			typpar.setDescription(rs.getString(7));
			typpar.setDateDebut(rs.getDate(8));
			typpar.setDateFin(rs.getDate(9));
			typpar.setDateCreation(rs.getDate(10));
			typpar.setDateMaj(rs.getDate(11));
			typpar.setMontConvEuro(rs.getString(12));
			typpar.setValeurDefaut(rs.getString(13));
			typpar.setLiblExtranet(rs.getString(14));
			typpar.setTextLong(rs.getString(15));
			typpar.setRegroupeValeurPossible(rs.getString(16));
			typpar.setParamInterne(rs.getString(17));
			typpar.setCodeInitialisation(rs.getString(18));
			typpar.setValeurInitialisation(rs.getString(19));
			typpar.setTypoNomenc(rs.getString(20));
			typpar.setMaxiValeur(rs.getFloat(21));
			typpars.add(typpar);
		}
		return typpars;
	}

}
