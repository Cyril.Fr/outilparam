package com.gfi.outilParam.service;

import java.util.List;

import com.gfi.outilParam.business.Typpar;

public interface TypparService {
	List<Typpar> getColumnNames();

}
