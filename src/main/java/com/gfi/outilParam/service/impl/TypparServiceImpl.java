package com.gfi.outilParam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.gfi.outilParam.business.Typpar;
import com.gfi.outilParam.dao.TypparDao;
import com.gfi.outilParam.dao.impl.TypparDaoImpl;
import com.gfi.outilParam.service.TypparService;

@Service
public class TypparServiceImpl implements TypparService {

	private TypparDao typparDao = new TypparDaoImpl();

	@Override
	public List<Typpar> getColumnNames() {
		try {
			return typparDao.findAllColumnNames();
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

}
