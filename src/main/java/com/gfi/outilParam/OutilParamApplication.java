package com.gfi.outilParam;

import java.sql.Connection;
import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.gfi.outilParam.utils.DataConnect;

@SpringBootApplication
public class OutilParamApplication {

	public static void main(String[] args) {
		SpringApplication.run(OutilParamApplication.class, args);

		Connection connection = null;

		try {
			connection = DataConnect.getConnection();
			System.out.println("Connection : " + connection);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					DataConnect.closeConnection(connection);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
