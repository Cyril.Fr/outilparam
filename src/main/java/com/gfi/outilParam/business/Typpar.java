package com.gfi.outilParam.business;

import java.util.Date;

public class Typpar {

	private float id;
	private String codeTypePara;
	private float idCatePara;
	private float idNatuPara;
	private float idFormPara;
	private String libelle;
	private String description;
	private Date dateDebut;
	private Date dateFin;
	private Date dateCreation;
	private Date dateMaj;
	private String montConvEuro;
	private String valeurDefaut;
	private String liblExtranet;
	private String textLong;
	private String regroupeValeurPossible;
	private String paramInterne;
	private String codeInitialisation;
	private String valeurInitialisation;
	private String typoNomenc;
	private float maxiValeur;

	public Typpar() {
	}

	public Typpar(String codeTypePara, int idCatePara, int idNatuPara, int idFormPara, String libelle,
			String description, Date dateDebut, Date dateFin, Date dateCreation, Date dateMaj, String montConvEuro,
			String valeurDefaut, String liblExtranet, String textLong, String regroupeValeurPossible,
			String paramInterne, String codeInitialisation, String valeurInitialisation, String typoNomenc,
			int maxiValeur) {
		super();
		this.codeTypePara = codeTypePara;
		this.idCatePara = idCatePara;
		this.idNatuPara = idNatuPara;
		this.idFormPara = idFormPara;
		this.libelle = libelle;
		this.description = description;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.dateCreation = dateCreation;
		this.dateMaj = dateMaj;
		this.montConvEuro = montConvEuro;
		this.valeurDefaut = valeurDefaut;
		this.liblExtranet = liblExtranet;
		this.textLong = textLong;
		this.regroupeValeurPossible = regroupeValeurPossible;
		this.paramInterne = paramInterne;
		this.codeInitialisation = codeInitialisation;
		this.valeurInitialisation = valeurInitialisation;
		this.typoNomenc = typoNomenc;
		this.maxiValeur = maxiValeur;
	}

	public float getId() {
		return id;
	}

	public void setId(float id) {
		this.id = id;
	}

	public String getCodeTypePara() {
		return codeTypePara;
	}

	public void setCodeTypePara(String codeTypePara) {
		this.codeTypePara = codeTypePara;
	}

	public float getIdCatePara() {
		return idCatePara;
	}

	public void setIdCatePara(float f) {
		this.idCatePara = f;
	}

	public float getIdNatuPara() {
		return idNatuPara;
	}

	public void setIdNatuPara(float f) {
		this.idNatuPara = f;
	}

	public float getIdFormPara() {
		return idFormPara;
	}

	public void setIdFormPara(float f) {
		this.idFormPara = f;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Date getDateMaj() {
		return dateMaj;
	}

	public void setDateMaj(Date dateMaj) {
		this.dateMaj = dateMaj;
	}

	public String getMontConvEuro() {
		return montConvEuro;
	}

	public void setMontConvEuro(String montConvEuro) {
		this.montConvEuro = montConvEuro;
	}

	public String getValeurDefaut() {
		return valeurDefaut;
	}

	public void setValeurDefaut(String valeurDefaut) {
		this.valeurDefaut = valeurDefaut;
	}

	public String getLiblExtranet() {
		return liblExtranet;
	}

	public void setLiblExtranet(String liblExtranet) {
		this.liblExtranet = liblExtranet;
	}

	public String getTextLong() {
		return textLong;
	}

	public void setTextLong(String textLong) {
		this.textLong = textLong;
	}

	public String getRegroupeValeurPossible() {
		return regroupeValeurPossible;
	}

	public void setRegroupeValeurPossible(String regroupeValeurPossible) {
		this.regroupeValeurPossible = regroupeValeurPossible;
	}

	public String getParamInterne() {
		return paramInterne;
	}

	public void setParamInterne(String paramInterne) {
		this.paramInterne = paramInterne;
	}

	public String getCodeInitialisation() {
		return codeInitialisation;
	}

	public void setCodeInitialisation(String codeInitialisation) {
		this.codeInitialisation = codeInitialisation;
	}

	public String getValeurInitialisation() {
		return valeurInitialisation;
	}

	public void setValeurInitialisation(String valeurInitialisation) {
		this.valeurInitialisation = valeurInitialisation;
	}

	public String getTypoNomenc() {
		return typoNomenc;
	}

	public void setTypoNomenc(String typoNomenc) {
		this.typoNomenc = typoNomenc;
	}

	public float getMaxiValeur() {
		return maxiValeur;
	}

	public void setMaxiValeur(float f) {
		this.maxiValeur = f;
	}

	@Override
	public String toString() {
		return "Typpar [id=" + id + ", codeTypePara=" + codeTypePara + ", idCatePara=" + idCatePara + ", idNatuPara="
				+ idNatuPara + ", idFormPara=" + idFormPara + ", libelle=" + libelle + ", description=" + description
				+ ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", dateCreation=" + dateCreation + ", dateMaj="
				+ dateMaj + ", montConvEuro=" + montConvEuro + ", valeurDefaut=" + valeurDefaut + ", liblExtranet="
				+ liblExtranet + ", textLong=" + textLong + ", regroupeValeurPossible=" + regroupeValeurPossible
				+ ", paramInterne=" + paramInterne + ", codeInitialisation=" + codeInitialisation
				+ ", valeurInitialisation=" + valeurInitialisation + ", typoNomenc=" + typoNomenc + ", maxiValeur="
				+ maxiValeur + "]";

	}
}
