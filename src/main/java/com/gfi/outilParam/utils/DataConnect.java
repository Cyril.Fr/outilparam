package com.gfi.outilParam.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataConnect {
	/**
	 * method to recover the connection to the database
	 * 
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public static Connection getConnection() throws SQLException, ClassNotFoundException {

		String url = "jdbc:oracle:thin:@172.16.44.29:1521:IW6102";
		String user = "anis";
		String password = "anis";

		Class.forName("oracle.jdbc.driver.OracleDriver");

		Connection connection = DriverManager.getConnection(url, user, password);

		return connection;
	}

	/**
	 * method of closing the database connection
	 * 
	 * @throws SQLException
	 */
	public static void closeConnection(Connection connection) throws SQLException {
		connection.close();
	}
}
