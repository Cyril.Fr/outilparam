package com.gfi.outilParam.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gfi.outilParam.business.Typpar;
import com.gfi.outilParam.service.TypparService;

@RestController
@RequestMapping(TypparRestController.BASE_URL)
public class TypparRestController {

	public static final String BASE_URL = "/api/v1/typpars";
	
	private TypparService typparService;

	public TypparRestController(TypparService typparService) {
		this.typparService = typparService;
	}

	@GetMapping
	public List<Typpar> getTypparColumns() {
		return typparService.getColumnNames();
	}

}
